# ~*~ encoding: utf-8 ~*~
import os


basedir = os.path.abspath(os.path.dirname(__file__))

CSRF_ENABLED = True
SECRET_KEY = '_aE^yY&(7?PcjDXNGNu<f7M&Cut`-zeBn`z9Lnpw'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
