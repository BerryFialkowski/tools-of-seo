# ~*~ encoding: utf-8 ~*~
from __future__ import unicode_literals

from lxml import html


class Parser(object):

    def parse(self, page):
        raise NotImplemented


class GoogleSuggestionParser(Parser):

    def parse(self, page):
        tree = html.fromstring(page)
        nodes = tree.xpath('*/suggestion/@data')
        return nodes
