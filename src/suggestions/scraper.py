# ~*~ encoding: utf-8 ~*~
import logging
import string

from itertools import product

from downloaders import RequestsDownloader
from engines import GoogleSuggestionEngine


class Scraper(object):

    default_engines = [GoogleSuggestionEngine()]
    default_downloader = RequestsDownloader()

    def __init__(self, engines=None, downloader=None):
        self.engines = engines or self.default_engines
        self.downloader = downloader or self.default_downloader

    def get_keyword_siblings(self, keyword, with_digits=False):
        # return keyword without changes
        yield keyword

        # create suffixes chars list (string)
        suffixes = string.lowercase + ' '
        if with_digits:
            suffixes += string.digits

        # generate keywords with suffixes
        for prefix, suffix in product(suffixes, suffixes):
            yield '{} {}'.format(keyword, suffix)
            yield '{} {}'.format(prefix, keyword)
            yield '{} {} {}'.format(prefix, keyword, suffix)

    def scrape_suggestions(self, sibling):
        for i, engine in enumerate(self.engines):
            try:
                page = self.downloader.load(sibling, engine)
            except:
                # logging.error('Can not download page for sibling "{}"'
                #               .format(sibling))
                return []
            #
            if not page:
                # logging.error('Can not download page for sibling "{}"'
                #               .format(sibling))
                return []
            else:
                return engine.get_suggestions(page)
